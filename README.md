# macOS

Steps and configurations that I use to setup a new Mac.

## 1. Install Homebrew

```shell
/usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
```

## 2. Install packages

```shell
git clone https://gitlab.com/jdrpereira/macos.git
cd macOS
./install
```

## 3. Install dotfiles

## 4. Setup git GPG signing

```shell
gpg --list-keys

git config --global gpg.program $(which gpg)
git config --global user.signingkey <your key ID>
git config --global commit.gpgsign true

git config --[global|local] user.email <your no-reply email>
```