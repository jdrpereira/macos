tap "caarlos0/tap"
tap "golangci/tap"
tap "homebrew/bundle"
tap "homebrew/cask"
tap "homebrew/cask-drivers"
tap "homebrew/cask-fonts"
tap "homebrew/cask-versions"
tap "homebrew/core"
tap "homebrew/services"
tap "joaodrp/tap"
# Cryptography and SSL/TLS Toolkit
brew "openssl@1.1"
# Record and share terminal sessions
brew "asciinema"
# Bourne-Again SHell, a UNIX command interpreter
brew "bash"
# Clone of cat(1) with syntax highlighting and Git integration
brew "bat"
# Statistics utility to count lines of code
brew "cloc"
# Cross-platform make
brew "cmake"
# GNU File, Shell, and Text utilities
brew "coreutils"
# Get a file from an HTTP, HTTPS or FTP server
brew "curl"
# Good-lookin' diffs with diff-highlight and more
brew "diff-so-fancy"
# YAML Parser
brew "libyaml"
# Isolated development environments using Docker
brew "docker-compose"
# Minify and secure Docker images
brew "docker-slim"
# Modern replacement for 'ls'
brew "exa"
# Perl lib for reading and writing EXIF metadata
brew "exiftool"
# Simple, fast and user-friendly alternative to find
brew "fd"
# Collection of GNU find, xargs, and locate
brew "findutils"
# Command-line fuzzy finder written in Go
brew "fzf"
# GNU internationalization (i18n) and localization (l10n) library
brew "gettext", link: true
# Distributed revision control system
brew "git"
# Command-line option parsing utility
brew "gnu-getopt"
# GNU implementation of the famous stream editor
brew "gnu-sed"
# GNU version of the tar archiving utility
brew "gnu-tar"
# GNU implementation of time utility
brew "gnu-time"
# GNU Transport Layer Security (TLS) Library
brew "gnutls"
# GNU Pretty Good Privacy (PGP) package
brew "gnupg"
# Open source programming language to build simple/reliable/efficient software
brew "go"
# Image processing tools collection
brew "graphicsmagick"
# Graph visualization software from AT&T and Bell Labs
brew "graphviz"
# GNU grep, egrep and fgrep
brew "grep"
# The Kubernetes package manager
brew "helm"
# User-friendly cURL replacement (command-line HTTP client)
brew "httpie"
# Curl statistics made simple
brew "httpstat"
# C/C++ and Java libraries for Unicode and globalization
brew "icu4c"
# Tools and libraries to manipulate images in many formats
brew "imagemagick"
# Calculate various network masks, etc. from a given IP address
brew "ipcalc"
# Lightweight and flexible command-line JSON processor
brew "jq"
# Conversion library
brew "libiconv"
# Mac App Store command-line interface
brew "mas"
# Scalable distributed version control system
brew "mercurial"
# Replacement for ls, cp and other commands for object storage
brew "minio-mc"
# NCurses Disk Usage
brew "ncdu"
# Port scanning utility for large networks
brew "nmap"
# Manage multiple Node.js versions
brew "nvm"
# Pinentry for GPG on Mac
brew "pinentry-mac"
# Manage compile and link flags for libraries
brew "pkg-config"
# Object-relational database system
brew "postgresql@10"
# Show ps output as a tree
brew "pstree"
# Python version management
brew "pyenv"
# Ruby version manager
brew "rbenv"
# Alternative to backtracking PCRE-style regular expression engines
brew "re2"
# Persistent key-value database, with built-in net interface
brew "redis"
# Search tool like grep and The Silver Searcher
brew "ripgrep"
# Collection of tools for managing UNIX services
brew "runit"
# Static analysis and lint tool, for (ba)sh scripts
brew "shellcheck"
# Add a public key to a remote machine's authorized_keys file
brew "ssh-copy-id"
# User interface to the TELNET protocol (built from macOS Sierra sources)
brew "telnet"
# Simplified and community-driven man pages
brew "tldr"
# Display directories as trees (with optional color/HTML output)
brew "tree"
# Extract, view, and test RAR archives
brew "unrar"
# HTTP load testing tool and library
brew "vegeta"
# Vi 'workalike' with many additional features
brew "vim"
# Executes a program periodically, showing output fullscreen
brew "watch"
# Internet file retriever
brew "wget"
# JavaScript package manager
brew "yarn"
# Tracks most-used directories to make cd smarter
brew "z"
# UNIX shell (command interpreter)
brew "zsh"
# Like gofmt, but for JSON files
brew "caarlos0/tap/jsonfmt"
# Fast linters runner for Go.
brew "golangci/tap/golangci-lint"
cask "1password"
cask "1password-cli"
cask "adoptopenjdk"
cask "appcleaner"
cask "chromedriver"
cask "dash"
cask "dbeaver-community"
cask "docker"
cask "firefox"
cask "font-source-code-pro"
cask "goland"
cask "google-chrome"
cask "istat-menus"
cask "iterm2"
cask "keka"
cask "keybase"
cask "kindle"
cask "little-snitch"
cask "logitech-options"
cask "muzzle"
cask "ngrok"
cask "omnigraffle"
cask "postman"
cask "proxyman"
cask "qlcolorcode"
cask "qlimagesize"
cask "qlmarkdown"
cask "qlprettypatch"
cask "qlstephen"
cask "quicklook-csv"
cask "quicklook-json"
cask "rubymine"
cask "slack"
cask "spotify"
cask "vagrant"
cask "virtualbox"
cask "visual-studio-code"
cask "vlc"
cask "yubico-authenticator"
cask "yubico-yubikey-manager"
cask "yubico-yubikey-personalization-gui"
cask "zoomus"
mas "CopyClip", id: 595191960
mas "Magnet", id: 441258766
mas "Okta Extension App", id: 1439967473
mas "Xcode", id: 497799835
